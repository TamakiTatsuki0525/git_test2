using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// １次元配列でデータを保存
/// </summary>
public class Testsc : MonoBehaviour
{
    int[] NumList = new int[10];
    public Test2cs test2;

    // Start is called before the first frame update
    void Start()
    {
        
        for (int i = 0; i < 10;i++)
        {
            NumList[i] = 1;
            //文章を追加
            if (i == 5)
            {
                NumList[i] = 55;
            }
            PlayerPrefs.SetInt("NumList" + i, NumList[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            for (int i = 0; i < 10; i++)
            {
                if (PlayerPrefs.HasKey("NumList" + i))
                {
                    // 存在する
                    NumList[i] = PlayerPrefs.GetInt("NumList" + i, 1000);
                    Debug.Log(NumList[i]);
                }
                else
                {
                    // 存在しない
                }


            }
        }
    }
}
